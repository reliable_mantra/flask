FROM python:3.7.3-slim-stretch

WORKDIR /app

COPY requirements.txt requirements.txt

RUN pip install -r requirements.txt

ENV PYTHONUNBUFFERED="true"
ENV PYTHONDONTWRITEBYTECODE="true"

COPY . .

EXPOSE 8000

CMD ["gunicorn", "snakeeyes.app:create_app()", "-b 0.0.0.0:8000", "--reload", "--access-logfile", "-", "--workers", "4"]
